1.  IP集成开发，添加ZYNQ7 Processing System，进行参数设置，创建gpio，设置为All inputs，宽度设为1（只用到一个引脚），打开中断，自动连线，修改gpio口的名称为keys，添加gpio，设置为All outputs，宽度设为4，自动连线，优化布局，修改gpio名称为leds,在ZYNQ7 Processing System里面打开中断连接，把IRQ_F2P连接到gpio口的中断口，检查设计规则，保存。
2.  创建HDL文件，Generate Output Products，创建约束文件。
3.  综合实现，生成比特流文件。
4.  导出硬件，打开SDK进行C语言编程，创建名为key_test的工程，使用Hello World的模板，导入gpio口的例程，修改定时器的延迟时间。
5.  重新写入FPGA的比特流文件，执行程序，打开putty软件，把端口改为COM5，速度为115200，重新执行程序，按下PL端的key1，就会显示执行成功。